package mati.sapmovies.management;

import mati.sapmovies.management.model.PendingReview;
import mati.sapmovies.management.model.ValidationResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.concurrent.CompletableFuture;

@RestController
public class ReviewController {

	private final ValidationService validation;

	public ReviewController(@Autowired ValidationService validation) {
		this.validation = validation;
	}

	@RequestMapping(value = "/reviews", method = RequestMethod.POST)
	public ResponseEntity<ValidationResult> create(@Valid @RequestBody PendingReview pendingReview) throws InterruptedException {
		CompletableFuture<ValidationResult> completableFuture = validation.validate(pendingReview);
		ValidationResult result = completableFuture.join();
		return ResponseEntity.ok(result);
	}

	@ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public ValidationResult handleException(MethodArgumentNotValidException exception) {
		ValidationResult result = new ValidationResult();
		for (ObjectError error : exception.getBindingResult().getAllErrors()) {
			result.getMessages().add(error.getDefaultMessage());
		}
		return result;
	}

}
