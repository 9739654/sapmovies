package mati.sapmovies.management.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Pattern;

public class MatchRegexValidator implements ConstraintValidator<MatchRegex, String> {

	private MatchRegex annotation;

	@Override
	public void initialize(MatchRegex constraintAnnotation) {
		this.annotation = constraintAnnotation;
	}

	@Override
	public boolean isValid(String object, ConstraintValidatorContext context) {
		if (object != null && Pattern.compile(annotation.value()).matcher(object).matches()) {
			return true;
		} else {
			context.disableDefaultConstraintViolation();
			context.buildConstraintViolationWithTemplate(annotation.message())
					.addConstraintViolation();
			return false;
		}
	}
}