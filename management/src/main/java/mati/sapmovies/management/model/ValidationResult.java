package mati.sapmovies.management.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Arrays;
import java.util.List;

public class ValidationResult {

	private boolean valid;

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private List<String> messages;

	public ValidationResult() {

	}

	public ValidationResult(List<String> messages) {
		this.valid = messages == null;
		this.messages = messages;
	}

	public boolean isValid() {
		return valid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}

	public List<String> getMessages() {
		return messages;
	}

	public void setMessages(List<String> messages) {
		this.messages = messages;
	}

	private static final ValidationResult validResult;

	static {
		validResult = new ValidationResult();
		validResult.valid = true;
	}

	public static ValidationResult valid() {
		return validResult;
	}

	public static ValidationResult notValid(String... messages) {
		return new ValidationResult(messages.length == 0 ? null : Arrays.asList(messages));
	}
}
