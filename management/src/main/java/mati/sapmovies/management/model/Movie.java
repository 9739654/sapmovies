package mati.sapmovies.management.model;

import mati.sapmovies.management.validation.MatchRegex;
import org.hibernate.validator.constraints.Length;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;

import java.util.Date;
import java.util.List;

public class Movie {

	@Id
	private String id;

	@Length(min = 3, max = 50, message = "Title length should be 3 to 50 chars. ")
	@MatchRegex(value = "[a-zA-Z]+", message = "Title can contain only letters.")
	private String title;

	private double rating;

	private String director;

	private List<String> actors;

	@CreatedDate
	private Date createdAt;

	public String getId() {
		return id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public double getRating() {
		return rating;
	}

	public void setRating(double rating) {
		this.rating = rating;
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public List<String> getActors() {
		return actors;
	}

	public void setActors(List<String> actors) {
		this.actors = actors;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
}
