package mati.sapmovies.management.model;

import org.springframework.data.annotation.CreatedDate;

import java.util.Date;

public class Review {

	private String text;

	@CreatedDate
	private Date createdAt;

	public Review() {

	}

	public Review(PendingReview review) {
		this.text = review.getText();
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
}
