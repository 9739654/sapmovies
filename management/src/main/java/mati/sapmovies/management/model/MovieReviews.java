package mati.sapmovies.management.model;

import org.springframework.data.annotation.Id;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

public class MovieReviews {

	@Id
	private String id;

	@NotNull
	private String movieId;

	private List<Review> reviews;

	public MovieReviews() {

	}

	public MovieReviews(String movieId) {
		this.movieId = movieId;
	}

	public String getId() {
		return id;
	}

	public String getMovieId() {
		return movieId;
	}

	public void setMovieId(String movieId) {
		this.movieId = movieId;
	}

	public List<Review> getReviews() {
		if (reviews == null) {
			reviews = new ArrayList<>();
		}
		return reviews;
	}

	public void setReviews(List<Review> reviews) {
		this.reviews = reviews;
	}
}
