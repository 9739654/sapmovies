package mati.sapmovies.management.model;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

public class PendingReview {

	@NotNull(message = "Movie id required.")
	private String movieId;

	@NotNull(message = "Text cannot be empty.")
	@Length(min=1, message = "Text lenght should be greater than 1.")
	private String text;

	public String getMovieId() {
		return movieId;
	}

	public void setMovieId(String movieId) {
		this.movieId = movieId;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
}
