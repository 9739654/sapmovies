package mati.sapmovies.management;

import mati.sapmovies.management.model.MovieReviews;
import mati.sapmovies.management.model.PendingReview;
import mati.sapmovies.management.model.Review;
import mati.sapmovies.management.model.ValidationResult;
import mati.sapmovies.management.repos.MovieRepository;
import mati.sapmovies.management.repos.MovieReviewsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

@Service
public class ValidationService {

	private final RestTemplate restTemplate;
	private final MovieReviewsRepository reviewsRepository;
	private final MovieRepository movieRepository;
	private final DiscoveryClient discovery;

	public ValidationService(@Autowired RestTemplateBuilder templateBuilder, @Autowired MovieReviewsRepository reviewsRepository, @Autowired MovieRepository movieRepository, @Autowired DiscoveryClient discovery) {
		this.restTemplate = templateBuilder.build();
		this.reviewsRepository = reviewsRepository;
		this.movieRepository = movieRepository;
		this.discovery = discovery;
	}

	@Async
	public CompletableFuture<ValidationResult> validate(PendingReview pendingReview) throws InterruptedException {
		final String movieId = pendingReview.getMovieId();
		boolean movieExists = movieRepository.exists(movieId);
		if (!movieExists) {
			ValidationResult result = ValidationResult.notValid("Movie id does not exist.");
			return CompletableFuture.completedFuture(result);
		}

		List<ServiceInstance> instances = discovery.getInstances("reviews");
		if (instances == null || instances.size() < 1) {
			StringBuilder sb = new StringBuilder();
			for (String s : discovery.getServices()) {
				sb.append(s).append(',');
			}
			if (discovery.getServices().size() > 0) {
				sb.deleteCharAt(sb.lastIndexOf(","));
			}
			return CompletableFuture.completedFuture(
					ValidationResult.notValid("Could not connect to reviews. Available services: " + sb.toString()));
		}

		String validationUrl = instances.get(0).getUri().toString() + "/validate";
		ValidationResult result = restTemplate.postForObject(validationUrl, pendingReview, ValidationResult.class);

		if (result.isValid()) {
			Optional<MovieReviews> maybeReviews = reviewsRepository.findByMovieId(movieId);
			MovieReviews reviews = maybeReviews.orElseGet(() -> new MovieReviews(movieId));
			reviews.getReviews().add(new Review(pendingReview));
			reviewsRepository.save(reviews);
		}

		// async simulation
		Thread.sleep(1000);
		return CompletableFuture.completedFuture(result);
	}

	private void validationServiceNotFound() {

	}

}
