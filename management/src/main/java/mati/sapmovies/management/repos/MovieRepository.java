package mati.sapmovies.management.repos;

import mati.sapmovies.management.model.Movie;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface MovieRepository extends MongoRepository<Movie, String> {
}
