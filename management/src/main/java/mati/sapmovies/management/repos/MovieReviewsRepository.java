package mati.sapmovies.management.repos;

import mati.sapmovies.management.model.MovieReviews;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.Optional;

@RepositoryRestResource(path = "movieReviews", collectionResourceRel = "review")
public interface MovieReviewsRepository extends MongoRepository<MovieReviews, String> {
	Optional<MovieReviews> findByMovieId(@Param("id") String id);
}
