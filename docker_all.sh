#!/usr/bin/env bash

cd discovery
mvn package dockerfile:build
cd ../management
mvn package dockerfile:build
cd ../reviews
mvn package dockerfile:build
cd ..