package mati.sapmovies.reviews.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Arrays;
import java.util.List;

public class Result {

	private final boolean valid;

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private final List<String> messages;

	private Result(List<String> messages) {
		this.messages = messages;
		valid = messages == null;
	}

	public boolean isValid() {
		return valid;
	}

	public List<String> getMessages() {
		return messages;
	}

	private static Result validResult = new Result( null);

	public static Result valid() {
		return validResult;
	}

	public static Result notValid(String... messages) {
		return new Result(messages.length == 0 ? null : Arrays.asList(messages));
	}


}
