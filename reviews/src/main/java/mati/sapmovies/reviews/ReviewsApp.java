package mati.sapmovies.reviews;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableConfigurationProperties(Config.class)
@EnableDiscoveryClient
@SpringBootApplication
public class ReviewsApp {

	public static void main(String[] args) {
		SpringApplication.run(ReviewsApp.class, args);
	}

}

