package mati.sapmovies.reviews;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

@Configuration
@ConfigurationProperties(prefix = "reviews")
public class Config {

	private List<String> bannedWords;

	public List<String> getBannedWords() {
		return bannedWords;
	}

	public void setBannedWords(List<String> bannedWords) {
		this.bannedWords = bannedWords;
	}

	public static class BannedWords extends ArrayList<String> {
		public BannedWords(List<String> bannedWords) {
			super(bannedWords);
		}
	}

	@Bean
	public BannedWords bannedWords() {
		return new BannedWords(getBannedWords());
	}

}
