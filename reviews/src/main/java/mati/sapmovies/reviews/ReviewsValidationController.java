package mati.sapmovies.reviews;

import mati.sapmovies.reviews.model.Result;
import mati.sapmovies.reviews.model.Review;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ReviewsValidationController {

	private final Config.BannedWords bannedWords;

	public ReviewsValidationController(@Autowired Config.BannedWords bannedWords) {
		this.bannedWords = bannedWords;
	}

	@RequestMapping(value = "/validate", method = RequestMethod.POST)
	public Result validate(@RequestBody Review review) {

		String text = review.getText().toLowerCase();
		for (String bannedWord : bannedWords) {
			if (text.contains(bannedWord)) {
				return Result.notValid("Text contains banned words");
			}
		}

		return Result.valid();
	}

}
