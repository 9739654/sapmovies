# sapmovies

## Create docker images

`./docker_all.sh` or `mvn package dockerfile:build` for each project

## Start

`cd` into projects root dir, run `docker-compose up -d`. Remove `-d` arg to show output.

## Movie management

- Get list of movies

`curl http://localhost:8080/movies`

- Get list of movies sorted by rating

`curl http://localhost:8080/movies?sort=rating,desc`

- Delete a movie

`curl -X DELETE http://localhost:8080/movies/{movieId}`

- Add movie

`curl -X POST -H "Content-Type:application/json" -d "{\"title\":\"No Country For Old Man\"}" localhost:8080/movies`

Use `PUT`, `PATCH`, and `DELETE` to either replace, update, or delete existing records.

- Add movie review

`curl -X POST -H "Content-Type:application/json" -d "{\"text\":\"nudny film\", \"movieId\":\"...\"}" localhost:8080/movieReviews`

Returns `{ "valid": true }` if review was accepted or `{ "valid": false, "messages": [...] }` otherwise.

- List all movie reviews

`curl localhost:8080/movieReviews`

- Get reviews by movie id

`curl localhost:8080/movieReviews/search/findByMovieId?id={movieId}`